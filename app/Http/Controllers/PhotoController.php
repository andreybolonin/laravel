<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // login page
        print_r('Put your phoneNumber');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($phoneNumber)
    {
        if ($phoneNumber && is_int((int)$phoneNumber)) {

            // generate smsCode
            $password = rand(1000, 9999);

            // save password and phoneNumber in db
            $user = User::where('phoneNumber', $phoneNumber)->first();
            if (!$user) {
                $user = new User();
                $user->phoneNumber = $phoneNumber;
            }
            $user->password = bcrypt($password);
            $user->save();

            // show phoneNumber and password
            echo('phoneNumber: ' . $phoneNumber);
            echo PHP_EOL;
            echo('password: ' . $password);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function check($phoneNumber, $password)
    {
        if (Auth::attempt(['phoneNumber' => $phoneNumber, 'password' => $password])) {
//             profile view
            var_dump(Auth::user());
        } else {
            // auth error
            print_r('Auth error');
        }
    }

    /**
     * Profile edit
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Request $request)
    {
        $user = Auth::user();
        if ($user && Auth::viaRemember() && $request->is('post')) {
            $user->phoneNumber = $request->get('phoneNumber');
            $user->name = $request->get('name');
            $user->dob = $request->get('dob');
            $user->city = $request->get('city');
            $user->save();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function listUsers()
    {
        $user = Auth::user();
        if ($user && Auth::viaRemember()) {
            $users = User::all();
            var_dump($users);
        }
    }
}
