<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'PhotoController@index');

Route::get('/edit', 'PhotoController@edit');

Route::get('/list', 'PhotoController@listUsers');

Route::get('create/{phoneNumber}', 'PhotoController@create');

Route::get('check/{phoneNumber}/{password}', 'PhotoController@check');

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

//Route::resource('auth', 'PhotoController');
//
//Route::resource('auth', 'PhotoController',
//    ['only' => ['index', 'show']]);
//
//Route::resource('auth', 'PhotoController',
//    ['except' => ['create', 'store', 'update', 'destroy']]);
