CREATE TABLE `user` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `phoneNumber` BIGINT(40) NOT NULL UNIQUE,
  `name` VARCHAR(40) NOT NULL,
  `dob` DATE NOT NULL,
  `city` VARCHAR(40) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `companyName` VARCHAR(80) DEFAULT NULL,
  `position` VARCHAR(80) DEFAULT NULL,
  `created_at` DATETIME,
  `updated_at` DATETIME
) ENGINE InnoDB;